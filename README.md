# Ansible Collection for Freqtrade

This collection deploys freqtrade bots on Linux hosts. SSH access is all you need.

It
 - installs Docker
 - adds an unpriviledged user
 - maintains a list of bots

Ansible >= 2.10 required. Currently tested on Debian.

## Installation
```
ansible-galaxy collection install -f git+https://gitlab.com/BenGTO/ansible_freqtrade.git
``` 

## Usage
Basic usage in a task list:
```
---
- name: Copy own strategies:
  copy:
    src: my_strat.py
    dest: /home/freqtrade/strategies/my_strat.py
    owner: freqtrade
    mode: 0644

- name: Install bots
  hosts: all
  tasks:
    - name: "Install bots"
      include_role:
        name: bots
```

### Strategies
A set of default strategies is included for testing purpose. For production, you will need to copy your strategies to a folder on the server first. And switch off the use of the default strategies, as shown below.

### Variables
See also `roles/bots/defaults/main/`.

Bots are listed in a variable `freqtrade_bots`. Most variables are obvious if you know the freqtrade configuration file.

```
---
freqtrade_cors_origins: ["http://localhost:5100","http://localhost:5101"]
freqtrade_use_use_default_strategies: false
freqtrade_bots:

  # Binance, dry run for comparision
  - dry_run: true
    stopped: false                                      # optional, default false
    max_open_trades: 4
    pairlists_template: freqtrade_binance_pairlists.json    
    exchange:
      stake_currency: "BUSD"
      available_capital: 2000
      name: "binance"
      key: "{{ binance_key }}"
      secret: "{{ binance_secret }}"
      pair_blacklist: []
      
    api:
      enabled: true
      port: 5100
      username: "{{ api_username }}"
      password: "{{ api_password }}"
      
    strategy:
      name: NostalgiaForInfinityX
      
    telegram:
      enabled: false

  # Kucoin
  - dry_run: false
    max_open_trades: 4
    pairlists_templase: freqtrade_kucoin_pairlists.json
    exchange:
      stake_currency: "USDT"
      tradable_balance_ratio: 0.85
      name: "kucoin"
      key: "{{ kucoin_key }}"
      secret: "{{ kucoin_secret }}"
      password: "{{ kucoin_password }}"
      pair_blacklist: []
    api:
      enabled: true
      port: 5101
      username: "{{ api_username }}"
      password: "{{ api_password }}"
    strategy:
      name: NostalgiaForInfinityX
    telegram:
      enabled: true
      token: "{{ kucoin_tg_token }}"
      chat_id: "{{ tg_chat_id }}"
```

## Configuration Templates

The freqtrade `config.json` is split into serveral templates in `roles/bots/templates`. You can copy a template
to the `template` directory of your playbook and adapt it to your needs.

Overwritable keys are:

 -  pairlists_template
 -  base_template
 -  exchange_template
 -  api_template
 -  strategy_template (**not** the strats file!)
